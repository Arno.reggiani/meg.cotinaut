# 2. Fusion 360 / CAO

Pour ce second module, nous avons pris en main **Fusion 360** d'Autodesk qui est un logiciel de CAO fonctionnant sur cloud et permettant notamment de faire de la conception 3D d'objets. Pour cet exercice nous avons donc réalisé l'objet de base que nous voulons transformer ou améliorer afin de pouvoir se l'approprier et mieux comprendre son fonctionnement ou la façon dont il est conçu.

Comme expliqué sur ma page de présentation, j'ai choisi pour objet la tablette de rangement de tableau de bord conçu pour les modèles T3 de VW.

## Mon objet

### Mesures

J'ai premièrement mesuré et relevé les différentes mesures nécessaires à sa conception, que vous pouvez trouver ci-dessous :

![Final measures](../images/module-2/finalproduct_measures.jpg)

### Matérialité

L'objet d'origine est fait d'un seul matériau, en plastique noir.

<br>

## Réalisation sur Fusion 360

### Le corps principal

**Pour commencer la réalisation de mon objet sur le logiciel, j'ai décidé de tout d'abord créer le gabarit principal, pour venir ensuite y découper les différents compartiments.**

1. Je débute en créant une esquisse (avec l'outil Sketch) sur le plan vu du dessus, je dessine un rectangle de 600x160mm en utilisant l'outil de  création de rectangles par leur centre.

2. Je viens ensuite extruder ce rectangle (avec l'outil Extrude) de -80mm vers le bas sur l'axe Z.

![Rectangle de base](../images/module-2/01_rectangle.png)
<img src="../images/module-2/02_extruder_rectangle.jpg" alt="Extruder rectangle" width="406 px"/>

3. Je crée une nouvelle esquisse sur la face droite de mon objet sur laquelle je viens créer une ligne que je dispose de l'angle inférieur gauche vers l'arrête opposée, à 50mm du coin supérieur droit (Outil Dimension). Je clique *Finish Sketch*.

![Esquisse ligne dessous](../images/module-2/03_lignesdroite.png)

4. J'extrude le triangle créé sur la totalité de mon objet, cela vient retiré une partie de cet objet afin de créer sa forme finale s'adaptant à l'angle du tableau de bord.

![Extruder dessous](../images/module-2/04_extruderdessous.png)
![Resultat corps principal](../images/module-2/05_dessous.jpg)


### Les compartiments

**Maintenant que le corps global de l'objet est terminé, je passe à la réalisation des compartiments sur la face supérieure en créant une nouvelle esquisse sur cette dernière.**

1. Je crée premièrement un rectangle à droite de ma face supérieure avec l'outil rectangle d'un coin à son opposé, que je dimensionne de 230mm sur sa longueur. Grâce à l'outil dimension, je le place à 10mm du bord supérieur, latéral et inférieur.

2. Je dessine le second rectangle central de 120mm de la même façon. Je le positionne à dimension égale des bords supérieurs et inférieurs et du premier rectangle.

3. Finalement je crée le dernier rectangle que je dimensionne en le plaçant à 10mm des bords supérieur et latéral gauche ainsi que du rectangle central. Pour le contraindre, je dimensionne également sa largeur à 30mm.

*Conseil : tant que votre forme n'apparaît pas en tracés noirs, cela signifie qu'elle n'est pas contrainte pour toutes ses dimensions.*

![Mesures de la partie supérieure](../images/module-2/06_mesuresfinales.jpg)

4. Pour introduire un élément paramétrique dans mon objet, je viens créer un nouveau paramètre qui permettra de faire varier le diamètre des portes-gobelets. Pour cela je vais dans l'onglet Modify -> Change parameters et je crée un nouveau paramètre comme celui-ci :

![Cercles parametriques](../images/module-2/07_parametres2.png)

5. Une fois le paramètre créé, je trace un cercle auquel je donne pour rayon "CircleDiameter", le nom de mon Paramètre.

6. Afin que le cercle reste toujours à la bonne distance des bords, je le place depuis son centre à "CircleDiameter / 2 + 10" mm du bord du rectangle supérieur, et à "CircleDiameter / 2 + 30"mm du bord latéral. *Je divise le diamètre par 2 afin d'obtenir son rayon sachant qu'il est placé depuis son centre.*

![Creation cercles](../images/module-2/08_cercledist2.png)

7. Je repète l'opération pour le second cercle, que je place à la même distance du rectangle supérieur et à "CircleDiameter / 2 + 30"mm du rectangle central à sa droite.

Après avoir créé les formes principales des compartiments, il est temps de dessiner les crans visibles dans le rectangle central.

8. Pour cela, dans mon esquisse actuelle, je me place sur le coin inférieur droit (ou gauche, peu importe) du rectangle au centre de ma console. Avec l'outil FitPointSpline je crée une forme conique en 3 points, sa base de 10mm (grâce à l'outil Dimension) sur l'arête du rectangle et son sommet à 10mm de la base et à distance égale des deux points précédents. Pour faciliter sa position à distance égale je dessine une ligne que je place au centre des deux points grâce à l'outil MidPoint, et je glisse le sommet sur cette ligne.

![Creation crans](../images/module-2/09_crans.png)

9. J'utilise la fonction Rectangular Pattern dans l'onglet Create pour venir répéter cette forme 3 fois à 28mm de distance chacune sur la longueur du rectangle. Pour cela, je sélectionne ma forme dans Objects, l'arrête de mon rectangle dans Direction/s, Spacing comme type de distance, en quantité de 4 (car l'objet d'origine est compté dedans) et une distance unique de -28mm.

![Repetition crans](../images/module-2/10_4crans.png)

10. Pour miroiter ces 4 formes de l'autre côté, je place une ligne perpendiculaire à la largeur et au centre du rectangle (avec l'outil MidPoint une fois de plus), puis l'outil Mirror, avec pour objets les 4 formes et la ligne créée pour axe.

![Miroir crans](../images/module-2/11_mirroir.png)

**Maintenant que le dessin final des compartiments est terminé, il faut les extruder afin de leur donner la profondeur de l'objet original.**

11. Avec l'outil Extrude, j'extrude les 3 rectangles, les deux plus grands compartiments à -40mm et le plus petit à - 15mm ainsi que les deux cercles à -30mm.

![Extruder compartiments](../images/module-2/12_extrude2.jpg)

12. Afin de créer le fond positionné en angle dans le plus grand compartiment, je crée une nouvelle esquisse sur une des faces latérales intérieures comme montré ci-dessous. Je répète alors la même étape que lors de la création du corps principal, en traçant une ligne partant du coin inférieur gauche vers l'arrête opposée à 15mm du coin supérieur.  

![Sketch fond en angle](../images/module-2/13_sketchinside.png)
![Sketch fond suite](../images/module-2/14_sketchline.png)

13. Avec l'outil Extrude, j'extrude le triangle créé jusqu'à l'autre face, soit 230mm

*Conseil : faites attention à bien être en type d'opération "Join"*

![Extrude fond](../images/module-2/15_extruderinside.jpg)

**Pour venir créer l'arrondi des bords de l'objet, je commence tout d'abord par le corps principal.**

14. Pour rendre la sélection de toutes les arrêtes plus simple, je retourne à l'étape où celui-ci venait d'être terminé, en glissant sur ma TimeLine. Avec l'outil Fillet, j'arrondis toutes mes arrêtes à 5mm.

![TimeLine](../images/module-2/16_timeline.png)

![Fillet corps principal](../images/module-2/17_fillet.jpg)

15. Je retourne à la fin de ma TimeLine, et je sélectionne les bords de tous mes compartiments (excepté celui au centre pour l'instant) que j'arrondis à 4mm.

![Fillet compartiments](../images/module-2/18_fillet2.jpg)

**Si je ne m'occupe pas encore des bords du compartiment central, c'est parce qu'à cette étape de la conception, je réalise que j'ai oublié de créer le fond en angle comme je l'ai fait sur le compartiment d'à côté.**

16. Par conséquent, comme pour l'autre compartiment, je viens commencer une nouvelle esquisse sur la face latérale intérieure comme montré ci-dessous. Je trace une ligne horizontale de 140mm depuis le coin supérieur droit de ma face, puis une autre que je situe à 40mm de l'extrémité gauche de ma ligne précédente et 20mm de l'autre extrémité.

17. Je supprime ma ligne horizontale, et je trace deux autres lignes pour venir former un triangle que j'extrude ensuite jusqu'à la face opposée à 120mm en sélectionnant une fois de plus l'opération *Join* dans la fenêtre de paramètres de mon outil.

<img src="../images/module-2/19_sketchpente.jpg" alt="Sketch pente" width="200 px"/>
<img src="../images/module-2/20_sketchpente3.png" alt="Sketch pente suite" width="200 px"/>
<img src="../images/module-2/21_sketchpente4.png" alt="Sketch pente fin" width="200 px"/>
<img src="../images/module-2/22_extruderinside.jpg" alt="Extrude fond bis" width="200 px"/>


18. Finalement, je peux enfin arrondir les bords supérieurs et ceux du fonds de ce compartiment également à 4mm.

![Fillet superieur](../images/module-2/23_fillet.jpg)
![Fillet fond](../images/module-2/24_fillet2.jpg)

<br>

**Tada ! Et voilà, votre tablette de tableau de bord de Vanagon est terminée !**

![Final object](../images/module-2/finalproduct.jpg)

## Useful links

- [La tablette](https://www.serial-kombi.com/fr-FR/transporter-t3-t25-equipement-interieur-tablette-plastique-courte-sur-tableau-de-bord-5/1979-7/1992-60-5x16cm-n311395)
- [Télécharger Fusion 360](https://www.autodesk.com/products/fusion-360/personal)
