# 1. Documentation et configuration Git

Cette première semaine consistait en une introduction à la documentation, que nous allons partager tout le long du quadrimestre, et à l'utilisation du GitLab, un outil permettant de faciliter cette documentation et les updates que nous ferons chaque semaine.
Nous avons donc configuré dans un premier temps le Git sur nos ordinateurs personnels à l'aide de la création d'une clé SSH, afin de créer une copie locale du projet, cela nous a également permis de prendre en main le terminal et quelques bases de codage afin de créer notre site web personnel puis partager et documenter chaque semaine notre avancée en termes d'apprentissage et de conception de notre objet.

## Configuration Git

**1. L'installation Git**

La configuration du Git nécessite avant tout d'utiliser le Terminal de votre ordinateur. Si comme moi vous êtes sur Mac, il y a de grandes chances que vous l'ayez déjà, tout comme Git, qui était déjà installé sur le mien. Pour vérifier cela, il suffit d'entrer :
`git --version`

Il vous indiquera la version que vous possédez, et dans le cas contraire, que vous ne l'avez pas encore installé sur votre ordinateur.

La seconde étape consiste à s'identifier à notre compte GitLab en indiquant ces deux commandes :

`git config --global user.name "your_username"`

`git config --global user.email "your_email_address@example.com"`


**2. Création d'une clé SSH**

Pour créer une clé SSH, vous devez avant tout vérifier que vous posséder OpenSSH afin de communiquer avec GitLab grâce à la commande :
`ssh -V`

Et si vous possédez déjà une clé SSH en cherchant un répertoire "*/.ssh*" dans votre disque.
Sur le Finder de Mac, onglet Go, Go to Folder... puis :

![DossierSSH](../images/module-1/mod1_06_dossierssh.png)

Pour ma part, je possédais déjà OpenSSH et je n'avais jamais utilisé de SSH.
Par conséquent, après l'étape d'identification au GitLab et de vérifications, voici où j'en étais rendu :

![Configuration](../images/module-1/mod1_03_verification_ssh.png)

Pour créer votre clé SSH, vous pouvez choisir parmi plusieurs types de clé comme la ED25519, RSA ou encore DSA. J'ai choisi la première car elle est plus sûre et performante. Ainsi pour celle-ci, vous entrez :
`ssh-keygen -t ed25519 -C "<comment>"`

Dans *"comment"*, j'ai indiqué mon adresse mail afin d'identifier la clé, je n'ai pas mis de mots de passe (il faut laisser vide et appuyer sur entrée deux fois lorsqu'il est demandé une passphrase dans ce cas) et celle-ci est alors créée :

![Clé](../images/module-1/mod1_05_clessh.jpg)

**3. Connexion avec GitLab**

Une fois votre clé créée, vous la copiez avant la commande (sur Mac) :

`tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`

Vous entrez alors la clé copiée dans vos préférences SSH sur votre compte GitLab sur Internet.

**4. Clone du projet**

La dernière étape de configuration du GitLab est de créer une copie locale de votre référentiel distant sur votre ordinateur. Cela nous permet de travailler sur notre PC directement depuis un éditeur de textes plutôt qu'en ligne sur le GitLab, tout en ayant les deux reliés.

Premièrement, sur votre projet dans le GitLab, "*meg.cotinaut*" pour ma part, vous trouverez le code à copier pour un clone par SSH dans l'onglet "*Clone*".

Vous ouvrez un terminal depuis le dossier où vous voulez que la copie locale se situe et entrez la commande ci-dessous en remplaçant le dernier élément par celui copié sur votre projet dans l'étape précédente :

`git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/meg.cotinaut.git`

Attention, s'il vous demande une confirmation comme sur la copie d'écran ci-dessous, il faut bien rentrer "yes" puis appuyer sur entrée, car si comme moi vous appuyez uniquement sur entrée, le clone ne sera pas effectué. Vous pouvez voir l'erreur, puis la correction qui s'en suit :

![Clone](../images/module-1/mod1_08_clone.jpg)
![Clone fin](../images/module-1/mod1_08_clonedone.png)

Et voilà, votre clone est créé et vous êtes prêt à documenter !


## Documentation

**1. Depuis la copie locale sur votre ordinateur**

Le premier exercice de documentation consistait à créer notre page de présentation.
Pour documenter, vous pouvez maintenant soit le faire depuis votre ordinateur grâce au clone effectué, ou depuis le GitLab.
Depuis mon ordinateur, j'utilise le logicel d'édition de texte Atom pour modifier les fichiers "*.md*" qui est assez facile à prendre en main.

Je l'ai téléchargé [ici](https://atom.io).

![Atom](../images/module-1/mod1_09_atom.jpg)

Attention, vos images doivent peser le moins possible, pour cela il suffit de les redimensionner ou de les compresser en baissant le % de la qualité lors de l'export. J'ai effectué ces modifications via le logiciel Photoshop, mais il est possible de le faire depuis le Terminal également.


Une fois que vous avez édité votre fichier, pour enregistrer les modifications et les envoyer vers le réferentiel distant, vous aurez besoin de trois commandes :

`git add -A` ->
`git commit -m "comment"` ->
`git push`

La première permet de préparer et enregistrer tous les fichiers modifés/ajoutés, la seconde envoie alors ces derniers vers le projet d'origine sur le GitLab.
Dans "*comment*" vous pouvez alors indiquer la nature de la modification comme repère.

![Push](../images/module-1/mod1_10_push.jpg)

**2. Depuis le GitLab**

Si vous modifiez directement votre fichier depuis le GitLab, une fois l'édition terminée, il suffit de cliquer sur commit pour enregistrer les modifications, (sans oublier d'écrire la nature des modifications pour s'y retrouver !) pour qu'elles soient également enregistrées dans votre copie locale, entrer dans le Terminal de votre répertoire du projet :
`git -pull`

![Commit](../images/module-1/mod1_11_text-commit.png)
![Pull](../images/module-1/mod1_12_pull.jpg)

Et voilà, vous êtes prêts à documenter !

## Liens utiles

- [Configuration GitLab](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git)
- [Création clé SSH](https://docs.gitlab.com/ee/ssh/index.html)
- [Téléchargement Atom](https://atom.io)
- [Git Cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
