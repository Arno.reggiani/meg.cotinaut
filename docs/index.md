## **À PROPOS DE MOI**

![Mon avatar](images/photo_presentation.jpg)

Hello !
Moi c'est **Meg**, j'ai 22 ans et je suis une étudiante française en dernière année de Master à la Faculté d’architecture de la Cambre-Horta à l'ULB.


### Mon parcours

Je suis née en France et j'y ai grandi, dans un petit village de campagne au Nord-Est de la France. Sortie du lycée le bac en poche, j'ai étudié l'anglais pendant un an à la fac pour finir mon apprentissage de la langue, puis j'ai effectué ma licence à l'École Nationale Supérieure d'Architecture de Nancy. Accro au changement et à l'aventure, j'ai déménagé à Bruxelles l'an dernier pour y faire mon master et découvrir quelque chose de nouveau. Malgré que le diplôme sera bientôt dans ma poche, je ne pense pas faire architecte plus tard —peut-être architecte d'intérieur ?— et je ne sais toujours pas ce que je veux réellement, j'attends plutôt de voir ce que l'avenir me réserve !


### Mes réalisations

Voici quelques exemples de mes réalisations au cours de mes 4 ans d'étude en architecture. Vous pouvez accéder à mon portfolio en [*cliquant ici*](https://megcotinaut.wixsite.com/portfolio) !

![Aperçu de mes projets](images/photosprojet.jpg)


### Mes passions

Je n'ai pas de grande passion particulière à laquelle je me serai consacrée pendant des années, mais je suis plutôt du genre très **curieuse** à vouloir tout essayer, en apprendre un peu sur tout et découvrir toujours de nouvelles choses et activités. J'aime beaucoup l'art sous toutes ses formes, du cinéma au tatouage en passant par la photo, tout comme la randonnée, la cuisine ou la pâtisserie.

Mais ce que j'aime par dessus tout, c'est l'**aventure**. C'est pour cette raison que le voyage est sûrement ma première passion : partir dans l'inconnu à la découverte de nouvelles cultures, des paysages et rencontrer des gens. Cette année, j'ai pu faire d'un de mes plus grands rêves une réalité, et j'ai commencé la conversion d'un vieux van VW T3 de 1985 pour en faire une maison roulante afin de vivre plus simplement, librement, et partir à l'aventure en prennant la route pour un tour d'Europe dès l'an prochain. Et en plus de tout ça, cela me permet d'en apprendre beaucoup sur de nouveaux hobbys comme la mécanique, le travail du bois ou la conception d'intérieurs et de meubles.

<img src="images/avater_small.jpg" alt="Le van" width="450 px"/>
<img src="images/rando_presentation.jpg" alt="Randonnee" width="350 px"/>

<br>

## **MON OBJET**

![Objet choisi](images/objet_depart.jpg)

Pour cette option design, le challenge est de concevoir ou transformer un objet du quotidien qui nous tenait à coeur. Comme dit juste avant, étant en plein aménagement de Ralph, mon van, pour en faire ma maison, je dois construire et penser à un grand nombre d'objets qui seront alors mon quotidien une fois vagabonde. C'est pour cela que j'ai décidé de travailler sur **un objet pour cet aménagement**. 
Mon choix s'est porté sur **une console centrale adapté à mon tableau de bord** mais aussi aux dimensions de mon téléphone, mon thermos, et beaucoup moins imposante que l'originale, qui me permettrait d'avoir quand je suis au volant, mon café, mon téléphone, ma carte dépliée — et qui me vend déjà du rêve en pensant au nombre de cafés renversés sur notre carte pendant mes road-trips précédents...
